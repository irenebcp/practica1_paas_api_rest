var express=require('express'); //inclusión de express
var app=express();
var bodyParser = require('body-parser');
app.use(bodyParser.json());
var port=process.env.PORT || 3000; //Puerto para express
var baseMLabURL="https://api.mlab.com/api/1/databases/apitechuibcp/collections/";
var mLabAPIKey="apiKey=_SfJ7mSPf08Ku65f0Y-5KSULb-pqwrO9";
var requestJson=require('request-json');

app.listen(port); //ponemos a escuchar
console.log("API escuchando en el puerto " + port);

//callback. Pasamos función como parámetro, que gestionará la llamada.
app.get('/apitechu/v1',
function(req,res){  //petición y respuesta
  console.log("GET /apitechu/v1");
  res.send({
    "msg":"Bienvenido a la API de Tech University molona"
  }); //con esto ya se muestra en el postman
}
);

app.get('/apitechu/v1/users',
function(req,res){
  console.log("GET /apitechu/v1/users");
  res.sendFile('usuarios.json',{root:__dirname});
  //res.sendFile('./usuarios.json');//Deprecated
}
);
//Versión 2
app.get('/apitechu/v2/users',
function(req,res){
  console.log("GET /apitechu/v2/users");
  httpClient=requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");
  httpClient.get("user?"+mLabAPIKey,
    function(err,resMLab,body){
      var response=!err?body:{
        "msg":"Error obteniendo usuarios."
      }
      res.send(response);
    }
  )
}
);
//Ejercicio Cuentas
app.get('/apitechu/v2/users/:id/accounts',
function(req,res){
  console.log("GET /apitechu/v2/users/:id/accounts");
  ///account?q={"userid":2}&apiKey=_SfJ7mSPf08Ku65f0Y-5KSULb-pqwrO9
  httpClient=requestJson.createClient(baseMLabURL);
  console.log("Cliente creado para cuentas");
  var id = req.params.id;
  var query= 'q={"userid":'+id+'}';
  console.log(query);
  httpClient.get("account?"+query+"&"+mLabAPIKey,
    function(err,resMLab,body){
      var response=!err?body:{
        "msg":"Error obteniendo cuentas."
      }
      res.send(response);
    }
  )
}
);


app.get('/apitechu/v2/users/:id',
function(req,res){
  console.log("GET /apitechu/v2/users/:id");

  var id = req.params.id;
  var query = 'q={"id":'+id+'}';
  httpClient=requestJson.createClient(baseMLabURL);
  console.log("Cliente creado");
  httpClient.get("user?"+query+"&"+mLabAPIKey,
    function(err, resMLab, body) {
     if (err) {
       response = {
         "msg" : "Error obteniendo usuario."
       }
       res.status(500);
     } else {
       if (body.length > 0) {
         response = body[0];
       } else {
         response = {
           "msg" : "Usuario no encontrado."
         };
         res.status(404);
       }
     }
     res.send(response);
   }
  )
}
);
app.post('/apitechu/v1/users',
function(req,res){
  console.log("POST /apitechu/v1/users");
  console.log(req);

  console.log("first_name es " + req.body.first_name);
  console.log("last_name es " + req.body.last_name);
  console.log("country es " + req.body.country);

  var newUser = {
    "first_name": req.body.first_name,
    "last_name": req.body.last_name,
    "country": req.body.country
  };

  var users = require('./usuarios.json');
  users.push(newUser);
  //console.log("Usuario añadido con éxito.");
  writeUserDataToFile(users,'./usuarios.json');

  //res.send(users); //Trampa para ver los usuarios pero no lo consolida

  res.send(
    {
      "msg": "Usuario enviado con éxito."
    }
  )
}

);
//Borrar un usuario por id
app.delete('/apitechu/v1/users/:id', //podría ser [id] o $id...
function(req,res){
  console.log("DETELE /apitechu/v1/users/:id");
  var users = require('./usuarios.json');
  users.splice(req.params.id - 1,1);
  writeUserDataToFile(users,'./usuarios.json');
  res.send(
    {
      "msg":"Usuario borrado con éxito."
    }
  );
}

);
//Hemos sacado la función del post para reutilizar.
function writeUserDataToFile(data,urlFile){
  var fs = require('fs');
  var jsonUserData = JSON.stringify(data);

  fs.writeFile(
    urlFile,
    jsonUserData,
    "utf-8",
    function(err){
      if(err){
        console.log(err);
      }else{
        console.log("Usuario persistido");
      }
    }

  )
}

app.post('/apitechu/v1/monstruo/:p1/:p2',
function(req,res) {
  console.log("Parametros");
  console.log(req.params);

  console.log("Query string");
  console.log(req.query);

  console.log("Headers");
  console.log(req.headers);

  console.log("Body");
  console.log(req.body);
}
);
//Login - Ejercicio 2
app.post('/apitechu/v1/login',
function(req,res){
  console.log("POST /apitechu/v1/login");
  console.log("Parámetros en el body: ");
  console.log(req.body);
  var logUser = {
    "email": req.body.email,
    "password": req.body.password,
  };
  var msg="Login incorrecto."
  var logFile = './userlogin.json';
  var logUsers = require(logFile);
  for (user of logUsers){
    if((logUser.email==user.email)&&(logUser.password==user.password)){
      console.log("Encontrado");
      user.logged=true;
      console.log(user);
      //logUsers.push(user);
      writeUserDataToFile(logUsers,logFile); //Persistir
      msg="Login correcto.";
      break;
    }

  }
  res.send(
    {
      "mensaje": msg
    }
  );
}
);
//Versión 2 del login
app.post('/apitechu/v2/login',
 function(req, res) {
   console.log("POST /apitechu/v2/login");
   var email = req.body.email;
   var password = req.body.password;
   var query = 'q={"email": "' + email + '", "password":"' + password +'"}';
   console.log("query es " + query);
   httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         res.status(404);
         var response = {
           "mensaje" : "Login incorrecto, email y/o passsword no encontrados"
         }
         res.send(response);
       } else {
         console.log("Got a user with that email and password, logging in");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$set":{"logged":true}}';
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             res.status(200);
             var response = {
               "msg" : "Usuario logado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
);
//Versión 2 del logout
app.post('/apitechu/v2/logout/:id',
 function(req, res) {
   console.log("POST /apitechu/v2/logout/:id");    var query = 'q={"id": ' + req.params.id + '}';
   console.log("query es " + query);    httpClient = requestJson.createClient(baseMLabURL);
   httpClient.get("user?" + query + "&" + mLabAPIKey,
     function(err, resMLab, body) {
       if (body.length == 0) {
         var response = {
           "mensaje" : "Logout incorrecto, usuario no encontrado"
         }
         res.send(response);
       } else {
         console.log("Got a user with that id, logging out");
         query = 'q={"id" : ' + body[0].id +'}';
         console.log("Query for put is " + query);
         var putBody = '{"$unset":{"logged":""}}'
         httpClient.put("user?" + query + "&" + mLabAPIKey, JSON.parse(putBody),
           function(errPUT, resMLabPUT, bodyPUT) {
             console.log("PUT done");
             var response = {
               "msg" : "Usuario deslogado con éxito",
               "idUsuario" : body[0].id
             }
             res.send(response);
           }
         )
       }
     }
   );
 }
);

//Logout - Ejercicio 2
app.post('/apitechu/v1/logout',
function(req,res){
  console.log("POST /apitechu/v1/logout");
  console.log("Parámetros en el body: ");
  console.log(req.body);
  var logUser = {
    "id": req.body.id,
  };
  var msg="Logout incorrecto."
  var logFile = './userlogin.json';
  var logUsers = require(logFile);
  for (user of logUsers){
    if(logUser.id==user.id){
      console.log("Encontrado");
      //user.logged=false;
      delete user.logged;
      console.log(user);
      logUsers.splice(user.id-1,1);
      writeUserDataToFile(logUsers,logFile); //Persistir
      msg="Logout correcto con id: "+ user.id;
      break;
    }
  }
  res.send(
    {
      "mensaje": msg
    }
  );
}
);
